package self.azterix87.seraphim;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector3;

import java.util.Map;

/**
 * Created by azterix87 on 3/27/17.
 * Project SeraphimEngine
 */
public interface ParametricEntityFactory {
    Entity createEntity(String type, Vector3 position, Map<String, String> parameters);

    String[] getSupportedEntityTypes();
}
