package self.azterix87.seraphim;


import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.assets.Assets;

import java.util.Map;

/**
 * Created by azterix87 on 1/22/17.
 * Project SeraphimEngine
 */
public interface GameContext {
    Engine entityEngine();

    void addEntity(Entity entity);

    void removeEntity(Entity entity);

    Entity[] findEntitiesWithTag(String tag);

    void addGlobalController(Controller controller);

    void removeGlobalController(Controller controller);

    Camera getMainCamera();

    void setMainCamera(Camera camera);

    Assets assets();

    Entity raytrace(Vector2 start, Vector2 direction, float maxDistance, String tag);

    Entity raytrace(Vector2 start, Vector2 direction, float maxDistance);

    Entity spawnEntity(String entityType, float x, float y, Map<String, String> parameters);

    Entity spawnEntity(String entityType, Vector3 position, Map<String, String> parameters);

    void addInputProcessor(InputProcessor processor);

    void addInputProcessor(int index, InputProcessor processor);

    void removeInputProcessor(InputProcessor processor);
}
