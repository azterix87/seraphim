package self.azterix87.seraphim.tiledmap;

import java.util.Arrays;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public class Tile {
    public static final int SIZE = 32;
    public static int AIR_TILE_ID = 0;
    private String name;
    private String TextureAtlas;
    private String[] textures;

    public Tile(String name, String textureAtlas, String... textures) {
        this.name = name;
        TextureAtlas = textureAtlas;
        this.textures = textures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextureAtlas() {
        return TextureAtlas;
    }

    public void setTextureAtlas(String textureAtlas) {
        TextureAtlas = textureAtlas;
    }

    public String[] getTextures() {
        return textures;
    }

    public String getTexture() {
        return textures[0];
    }

    public String getTexture(int subId) {
        return getTexture();
    }

    public boolean hasMultiTextures() {
        return getTextures().length > 0;
    }

    @Override
    public String toString() {
        return "Tile{" +
                "name='" + name + '\'' +
                ", TextureAtlas='" + TextureAtlas + '\'' +
                ", textures=" + Arrays.toString(textures) +
                '}';
    }
}
