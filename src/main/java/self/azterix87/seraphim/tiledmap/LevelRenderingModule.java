package self.azterix87.seraphim.tiledmap;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Matrix4;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.systems.RenderingModule;

/**
 * Created by azterix87 on 5/2/17.
 * Project SeraphimEngine
 */
public class LevelRenderingModule implements RenderingModule {
    private ImmutableArray<Entity> levelEntities;
    private Level currentLevel;
    private int layerOffsetXMul = Tile.SIZE;
    private int layerOffsetYMul = Tile.SIZE;


    @Override
    public void addedToEngine(Engine entityEngine) {
        //Find all entities with "level" component
        levelEntities = entityEngine.getEntitiesFor(Family.all(Level.class).get());
    }

    @Override
    public void preRender(RenderContext renderContext, float dt) {
        //Find current level entity
        if (levelEntities.size() > 0) {
            currentLevel = Level.mapper.get(levelEntities.get(0));
        } else {
            currentLevel = null;
        }
    }

    @Override
    public void render(int sortingIndex, RenderContext renderContext, float dt) {
        //Return if there is no current level
        if (currentLevel == null) {
            return;
        }

        //Get tile registry of current level
        TileRegistry tileRegistry = currentLevel.getTileRegistry();

        //Get spriteBatch from RenderContext, set projection to camera and begin rendering
        SpriteBatch spriteBatch = renderContext.spriteBatch();
        spriteBatch.setProjectionMatrix(renderContext.getMainCamera().combined);
        spriteBatch.setTransformMatrix(new Matrix4());
        spriteBatch.enableBlending();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.begin();

        //Get layers of rooms with current sorting index
        for (Chunk chunk : currentLevel.getChunks()) {
            for (Layer layer : chunk.findLayersWithSortingIndex(sortingIndex)) {

                //layer coordinates in game units
                float layerX = chunk.getTileX() * Tile.SIZE + layer.getXOffset() * layerOffsetXMul;
                float layerY = chunk.getTileY() * Tile.SIZE + layer.getYOffset() * layerOffsetYMul;


                //Cycle through current layers tiles
                for (int tileX = 0; tileX < chunk.getWidth(); tileX++) {
                    for (int tileY = 0; tileY < chunk.getHeight(); tileY++) {
                        //Ignore air tiles, they don't need rendering, do they?
                        if (layer.getTileAt(tileX, tileY) == Tile.AIR_TILE_ID) {
                            continue;
                        }

                        //Get tile object by id
                        Tile tile = tileRegistry.getTile(layer.getTileAt(tileX, tileY));

                        //Find tiles texture atlas and get tile's texture from it
                        TextureAtlas atlas = renderContext.assets().getTextureAtlas(tile.getTextureAtlas());
                        TextureAtlas.AtlasRegion region = atlas.findRegion(tile.getTexture());


                        //Tile coordinates in game units
                        float x = tileX * Tile.SIZE;
                        float y = tileY * Tile.SIZE;

                        //Draw tile at correct position
                        if (region != null) {
                            spriteBatch.draw(region, layerX + x, layerY + y);
                        } else {
                            //In case atlas is missing this region, draw default texture
                            spriteBatch.draw(renderContext.assets().getDefaultTexture(), layerX + x, layerY + y, Tile.SIZE, Tile.SIZE);
                        }
                    }
                }
            }
        }

        //end rendering
        spriteBatch.end();
        spriteBatch.disableBlending();
    }

    @Override
    public void postRender(RenderContext renderContext, float dt) {

    }

    @Override
    public int getMinSortingIndex() {
        if (currentLevel != null) {
            return currentLevel.getMinSortingIndex();
        } else {
            return 0;
        }
    }

    @Override
    public int getMaxSortingIndex() {
        if (currentLevel != null) {
            return currentLevel.getMaxSortingIndex();
        } else {
            return 0;
        }
    }

    public int getLayerOffsetXMul() {
        return layerOffsetXMul;
    }

    public void setLayerOffsetXMul(int layerOffsetXMul) {
        this.layerOffsetXMul = layerOffsetXMul;
    }

    public int getLayerOffsetYMul() {
        return layerOffsetYMul;
    }

    public void setLayerOffsetYMul(int layerOffsetYMul) {
        this.layerOffsetYMul = layerOffsetYMul;
    }
}
