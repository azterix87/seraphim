package self.azterix87.seraphim.tiledmap;

import com.badlogic.ashley.core.Entity;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.collision.Collider;
import self.azterix87.seraphim.systems.collision.ColliderFactory;

/**
 * Created by azterix87 on 5/12/2017.
 * Project SeraphimEngine
 */
public class TileColliderFactory {
    public Entity getTileCollider(float x, float y) {
        Entity tileCollider = new Entity();

        //Add transform to entity
        Transform transform = new Transform();
        transform.localPosition.set(x, y, 0);
        tileCollider.add(transform);

        //Add collision box to entity
        Collider collider = ColliderFactory.getBoxCollider(Tile.SIZE, Tile.SIZE);
        tileCollider.add(collider);

        return tileCollider;
    }
}
