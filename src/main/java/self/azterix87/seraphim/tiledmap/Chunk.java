package self.azterix87.seraphim.tiledmap;


import com.badlogic.ashley.core.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public class Chunk {
    private List<Layer> layers = new ArrayList<>();
    private boolean[][] collisionMap;
    private List<Entity> chunkEntities = new ArrayList<>();
    private int width, height;
    private int tileX, tileY;

    public Chunk(int tileX, int tileY, int width, int height) {
        this.width = width;
        this.height = height;
        this.tileX = tileX;
        this.tileY = tileY;
    }

    public void addLayer(Layer layer) {
        layers.add(layer);
    }

    public void removeLayer(Layer layer) {
        layers.remove(layer);
    }

    public Layer getLayer(int index) {
        return layers.get(index);
    }

    public void addChunkEntity(Entity e) {
        chunkEntities.add(e);
    }

    public void removeChunkEntity(Entity e) {
        chunkEntities.remove(e);
    }

    public Entity[] getChunkEntities() {
        Entity[] result = new Entity[chunkEntities.size()];
        chunkEntities.toArray(result);
        return result;
    }

    public List<Layer> findLayersWithSortingIndex(int sortingIndex) {
        List<Layer> result = new ArrayList<>();
        for (Layer layer : layers) {
            if (layer.getSortingIndex() == sortingIndex) {
                result.add(layer);
            }
        }
        return result;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getTileX() {
        return tileX;
    }

    public void setTileX(int tileX) {
        this.tileX = tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void setTileY(int tileY) {
        this.tileY = tileY;
    }

    public boolean[][] getCollisionMap() {
        return collisionMap;
    }

    public void setCollisionMap(boolean[][] collisionMap) {
        this.collisionMap = collisionMap;
    }

    public int getMinSortingIndex() {
        int min = 0;

        boolean first = true;
        for (Layer layer : layers) {
            if (first) {
                min = layer.getSortingIndex();
                first = false;
            } else {
                min = Math.min(min, layer.getSortingIndex());
            }
        }

        return min;
    }

    public int getMaxSortingIndex() {
        int max = 0;

        boolean first = true;
        for (Layer layer : layers) {
            if (first) {
                max = layer.getSortingIndex();
                first = false;
            } else {
                max = Math.max(max, layer.getSortingIndex());
            }
        }

        return max;
    }

    public void onChunkAdded() {
        //todo
    }

    public void onChunkRemoved() {
        //todo
    }
}
