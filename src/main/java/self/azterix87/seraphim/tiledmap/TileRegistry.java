package self.azterix87.seraphim.tiledmap;

import com.badlogic.gdx.Gdx;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public class TileRegistry {
    private Tile[] tiles;

    public TileRegistry(int maxTiles) {
        tiles = new Tile[maxTiles];
        tiles[0] = new Tile("air", "null", "");
    }

    public void addTile(int id, Tile tile) {
        //Check if id is in bounds
        if (id >= tiles.length) {
            Gdx.app.error("@TileRegistry", "Can not add tile " + tile.getName() + " id " + id + " is out of bounds");
        }

        //If id is occupied with another tile, replace it and log message. else just add a tile
        if (tiles[id] != null) {
            Gdx.app.log("@TileRegistry", "Overriding tile [" + tiles[id].getName() + "] at id " +
                    id + " with tile [" + tile.getName() + "]");
        } else {
            Gdx.app.log("@TileRegistry", "Registered tile [" + tile.getName() + "] at id " + id);
        }

        tiles[id] = tile;
    }

    public Tile getTile(int id) {
        if (tiles[id] != null) {
            return tiles[id];
        } else {
            //If tile is not present, return 0 (air) tile
            Gdx.app.error("@TileRegistry", "No tile found at id " + id);
            tiles[id] = tiles[0];
            return tiles[0];
        }
    }

    public int getTileId(String tileName) {
        for (int i = 0; i < tiles.length; i++) {
            if (tiles[i].getName().equals(tileName)) {
                return i;
            }
        }

        Gdx.app.error("@TileRegistry", "No tile with name " + tileName + " was found");

        return 0;
    }

    public Tile getTile(String tileName) {
        return getTile(getTileId(tileName));
    }
}
