package self.azterix87.seraphim.tiledmap;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import self.azterix87.seraphim.GameContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 5/2/17.
 * Project SeraphimEngine
 */
public class Level extends Component {
    public static final ComponentMapper<Level> mapper = ComponentMapper.getFor(Level.class);
    private static final TileColliderFactory colFactory = new TileColliderFactory();
    private TileRegistry tileRegistry;
    private List<Chunk> chunks = new ArrayList<>();
    private GameContext gameContext;

    public Level(TileRegistry tileRegistry, GameContext gameContext) {
        this.tileRegistry = tileRegistry;
        this.gameContext = gameContext;
    }

    public Chunk getChunkAt(int tileX, int tileY) {
        for (Chunk chunk : chunks) {
            //Check if tile is within the chunk
            if (tileX >= chunk.getTileX() && tileX < chunk.getTileX() + chunk.getWidth()
                    && tileY >= chunk.getTileY() && tileY < chunk.getTileY() + chunk.getHeight()) {
                return chunk;
            }
        }

        return null;
    }

    public void addChunk(Chunk chunk) {
        Gdx.app.log("@level", "Chunk has been added at (" + chunk.getTileX() + ", " + chunk.getTileY() + ")");
        chunks.add(chunk);

        //Spawn collider entities for wall tiles
        spawnTileColliders(chunk);
    }

    public void spawnTileColliders(Chunk chunk) {
        //Get chunk collision map
        boolean[][] collisionMap = chunk.getCollisionMap();

        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {
                //If collision map is true at this point, spawn collider
                if (collisionMap[i][j]) {
                    //Coordinates = chunk coordinates + tile coordinates
                    float x = (chunk.getTileX() + i) * Tile.SIZE;
                    float y = (chunk.getTileY() + j) * Tile.SIZE;

                    //Create collider, spawn it and add it to chunkEntities, so it is removed when chunk is unloaded.
                    Entity collider = colFactory.getTileCollider(x, y);
                    chunk.addChunkEntity(collider);
                    gameContext.addEntity(collider);
                }
            }
        }
    }

    public void removeChunk(Chunk chunk) {
        chunks.remove(chunk);

        //Remove all entities assigned with this chunk
        for (Entity entity : chunk.getChunkEntities()) {
            gameContext.removeEntity(entity);
        }
    }

    public List<Chunk> getChunks() {
        return chunks;
    }

    public TileRegistry getTileRegistry() {
        return tileRegistry;
    }

    public void setTileRegistry(TileRegistry tileRegistry) {
        this.tileRegistry = tileRegistry;
    }

    public int getMinSortingIndex() {
        int min = 0;

        boolean first = true;
        for (Chunk chunk : chunks) {
            if (first) {
                min = chunk.getMinSortingIndex();
                first = false;
            } else {
                min = Math.min(min, chunk.getMinSortingIndex());
            }
        }

        return min;
    }

    public int getMaxSortingIndex() {
        int max = 0;

        boolean first = true;
        for (Chunk chunk : chunks) {
            if (first) {
                max = chunk.getMaxSortingIndex();
                first = false;
            } else {
                max = Math.min(max, chunk.getMaxSortingIndex());
            }
        }

        return max;
    }

}
