package self.azterix87.seraphim.tiledmap;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public class Layer {
    private int sortingIndex;
    private int xOffset;
    private int yOffset;
    private int[][] data;

    public Layer(int width, int height) {
        this(width, height, 0);
    }

    public Layer(int width, int height, int sortingIndex) {
        this.data = new int[width][height];
        this.sortingIndex = sortingIndex;
    }

    public int getXOffset() {
        return xOffset;
    }

    public void setXOffset(int xOffset) {
        this.xOffset = xOffset;
    }

    public int getYOffset() {
        return yOffset;
    }

    public void setYOffset(int yOffset) {
        this.yOffset = yOffset;
    }

    public int getTileAt(int x, int y) {
        return data[x][y];
    }

    public void setTileAt(int x, int y, int id) {
        data[x][y] = id;
    }

    public int getSortingIndex() {
        return sortingIndex;
    }

    public void setSortingIndex(int sortingIndex) {
        this.sortingIndex = sortingIndex;
    }
}
