package self.azterix87.seraphim;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public class Timer {
    private float startTime;
    private float duration;

    public Timer(float duration) {
        this.duration = duration;
    }

    public boolean isFinished() {
        return timeElapsed() >= duration;
    }

    public float timeElapsed() {
        return TimeUtil.seconds() - startTime;
    }

    public float timeLeft() {
        return duration - timeElapsed();
    }

    public float getProgress() {
        return Math.min(timeElapsed() / duration, 1);
    }

    public void reset() {
        startTime = TimeUtil.seconds();
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}
