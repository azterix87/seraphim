package self.azterix87.seraphim.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class AssetsLoader {
    private List<AssetType> assetTypes = new ArrayList<>();


    public void registerAssetType(AssetType assetType) {
        assetTypes.add(assetType);
        Gdx.app.log("@AssetsLoader", "Registered asset type " + assetType.getName() + " " +
                Arrays.toString(assetType.getExtensions()));
    }

    public void loadAssets(FileHandle assetsFolder, AssetManager assetManager) {
        if (!assetsFolder.exists() || !assetsFolder.isDirectory()) {
            Gdx.app.error("@AssetsLoader", "Error: " + assetsFolder + " is not a valid directory");
            return;
        }

        Gdx.app.log("@AssetsLoader", "Searching for assets in /" + assetsFolder.path() + "/ ...");

        for (AssetType assetType : assetTypes) {
            if (assetType.hasSpecialLoader()) {
                assetManager.setLoader(assetType.getAssetClass(), assetType.getAssetLoader());
            }

            List<FileHandle> files = new ArrayList<>();
            for (String extension : assetType.getExtensions()) {
                getContentsRecursive(assetsFolder, extension, files);
            }

            for (FileHandle file : files) {
                Gdx.app.log("@AssetsLoader", "Loading " + assetType.getName() + ": " + file.path());
                if (assetType.hasParameters()) {
                    assetManager.load(file.path(), assetType.getAssetClass(), assetType.getParameters());
                } else {
                    assetManager.load(file.path(), assetType.getAssetClass());
                }
                assetManager.finishLoading();
            }

            if (files.size() != 0) {
                Gdx.app.log("@AssetsLoader", "-- Loaded " + files.size() + " " + assetType.getName()
                        + (files.size() > 1 ? (assetType.getName().endsWith("s") ? "es" : "s --") : " --"));
            }
        }

    }

    private void getContentsRecursive(FileHandle folder, String suffix, List<FileHandle> result) {
        for (FileHandle file : folder.list()) {
            if (file.isDirectory()) {
                getContentsRecursive(file, suffix, result);
            } else {
                if (file.name().toLowerCase().endsWith(suffix.toLowerCase())) {
                    result.add(file);
                }
            }
        }
    }
}

