package self.azterix87.seraphim;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.assets.Assets;
import self.azterix87.seraphim.components.Tags;
import self.azterix87.seraphim.systems.collision.Collider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by azterix87 on 1/8/2017.
 * Project SeraphimEngine
 */
public class GameData implements RenderContext, GameContext {
    public final Engine entityEngine;
    public final SpriteBatch spriteBatch;
    public final FactoryManager factoryManager;
    public final Assets assets;
    private Camera camera;
    private List<Controller> controllers = new ArrayList<>();
    private ImmutableArray<Entity> taggedEntities;
    private InputMultiplexer input;

    public GameData(Game game, Assets assets, SpriteBatch spriteBatch) {
        this.entityEngine = new Engine();
        this.assets = assets;
        this.spriteBatch = spriteBatch;
        this.factoryManager = new FactoryManager();
        this.input = new InputMultiplexer();
        taggedEntities = entityEngine.getEntitiesFor(Family.all(Tags.class).get());
    }

    //BADCODE
    @Override
    public Entity raytrace(Vector2 start, Vector2 direction, float maxDistance, String tag) {
        Entity[] entities;
        if (tag == null) {
            ImmutableArray<Entity> e = entityEngine.getEntitiesFor(Family.all(Collider.class).get());
            entities = new Entity[e.size()];
            for (int i = 0; i < entities.length; i++) {
                entities[i] = e.get(i);
            }
        } else {
            entities = findEntitiesWithTag(tag);
        }

        return RayTracer.raytrace(start, direction, maxDistance, entities);
    }

    @Override
    public Entity[] findEntitiesWithTag(String tag) {
        List<Entity> entities = new ArrayList<>();
        for (Entity e : taggedEntities) {
            if (Tags.mapper.get(e).hasTag(tag)) {
                entities.add(e);
            }
        }

        Entity[] result = new Entity[entities.size()];
        entities.toArray(result);

        return result;
    }

    @Override
    public Entity raytrace(Vector2 start, Vector2 step, float maxDistance) {
        return raytrace(start, step, maxDistance, null);
    }

    @Override
    public Entity spawnEntity(String entityType, float x, float y, Map<String, String> parameters) {
        return spawnEntity(entityType, new Vector3(x, y, 0), parameters);
    }

    @Override
    public Entity spawnEntity(String entityType, Vector3 position, Map<String, String> parameters) {
        Entity e = factoryManager.createEntity(entityType, position, parameters);
        if (e != null) {
            entityEngine.addEntity(e);
            Gdx.app.debug("@GameData", "Entity [" + entityType + "] spawned at (" + position.x + ", " + position.y + ")");
        }
        return e;
    }

    @Override
    public void addInputProcessor(InputProcessor processor) {
        input.addProcessor(processor);
    }

    @Override
    public void addInputProcessor(int index, InputProcessor processor) {
        input.addProcessor(index, processor);
    }

    @Override
    public void removeInputProcessor(InputProcessor processor) {
        input.removeProcessor(processor);
    }

    private Texture createDefaultTexture() {
        Gdx.app.debug("@GameData", "Creating default texture 32x32");
        Pixmap pixmap = new Pixmap(32, 32, Pixmap.Format.RGBA8888);

        pixmap.setColor(Color.PURPLE);
        pixmap.fillRectangle(0, 0, 32, 32);
        pixmap.setColor(Color.BLACK);
        pixmap.fillRectangle(0, 0, 16, 16);
        pixmap.fillRectangle(16, 16, 16, 16);
        return new Texture(pixmap);
    }

    public void dispose() {
        spriteBatch.dispose();
        assets.dispose();
    }

    @Override
    public SpriteBatch spriteBatch() {
        return spriteBatch;
    }

    @Override
    public Camera getMainCamera() {
        return camera;
    }

    @Override
    public void setMainCamera(Camera camera) {
        this.camera = camera;
    }

    @Override
    public Engine entityEngine() {
        return entityEngine;
    }

    @Override
    public Assets assets() {
        return assets;
    }

    @Override
    public void addEntity(Entity entity) {
        entityEngine.addEntity(entity);
    }

    @Override
    public void removeEntity(Entity entity) {
        entityEngine.removeEntity(entity);
    }

    @Override
    public void addGlobalController(Controller controller) {
        controllers.add(controller);
        controller.onControllerAdded(this);
    }

    @Override
    public void removeGlobalController(Controller controller) {
        controllers.remove(controller);
        controller.onControllerRemoved(this);
    }

    public List<Controller> getControllers() {
        return controllers;
    }

    public InputProcessor getInputProcessor() {
        return input;
    }

}
