package self.azterix87.seraphim;

import com.badlogic.gdx.InputAdapter;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public abstract class InputAdapterController extends InputAdapter implements Controller {
    private int inputPriority = -1;

    public InputAdapterController() {

    }

    public InputAdapterController(int inputPriority) {
        this.inputPriority = inputPriority;
    }

    @Override
    public void onControllerAdded(GameContext gameContext) {
        if (inputPriority == -1) {
            gameContext.addInputProcessor(this);
        } else {
            gameContext.addInputProcessor(inputPriority, this);
        }
    }

    @Override
    public void onControllerRemoved(GameContext gameContext) {
        gameContext.removeInputProcessor(this);
    }
}
