package self.azterix87.seraphim.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

/**
 * Created by azterix87 on 2/27/17.
 * Project SeraphimEngine
 */
public abstract class GUIScreen implements Screen {
    protected GUI gui;

    public GUIScreen() {
        gui = new GUI();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        gui.render();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gui.getInputProcessor());
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }


    @Override
    public void resize(int width, int height) {
        gui.resize(width, height);
    }

    @Override
    public void dispose() {
        gui.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
