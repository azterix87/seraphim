package self.azterix87.seraphim.gui;


import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by azterix87 on 2/15/17.
 * Project SeraphimEngine
 */
public class BorderLayout implements Layout {
    public final Table TOP;
    public final Table BOTTOM;
    public final Table LEFT;
    public final Table RIGHT;
    public final Table CENTER;
    private final Table root;

    public BorderLayout() {
        root = new Table();
        root.setFillParent(true);
        root.setDebug(true);

        TOP = new Table();
        BOTTOM = new Table();
        LEFT = new Table();
        RIGHT = new Table();
        CENTER = new Table();

        //root.add(new TextButton("lolo", skin)).;

        root.add(TOP).fill().colspan(3);
        root.row();

        root.add(LEFT).fill();
        root.add(CENTER).expand().fill();
        root.add(RIGHT).fill();

        root.row();

        root.add(BOTTOM).fill().colspan(3);
    }

    @Override
    public WidgetGroup getRoot() {
        return root;
    }

}
