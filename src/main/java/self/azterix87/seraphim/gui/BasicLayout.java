package self.azterix87.seraphim.gui;

import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by azterix87 on 2/27/17.
 * Project SeraphimEngine
 */
public class BasicLayout implements Layout {
    private WidgetGroup root;

    public BasicLayout(WidgetGroup root) {
        this.root = root;
    }

    @Override
    public WidgetGroup getRoot() {
        return root;
    }
}
