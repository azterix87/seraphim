package self.azterix87.seraphim.gui;

import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by azterix87 on 2/15/17.
 * Project SeraphimEngine
 */
public interface Layout {
    WidgetGroup getRoot();
}
