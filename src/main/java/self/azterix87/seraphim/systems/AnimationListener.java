package self.azterix87.seraphim.systems;

/**
 * Created by azterix87 on 2/7/17.
 * Project SeraphimEngine
 */
public interface AnimationListener {
    void onAnimationStarted(String animation);

    void onAnimationPaused(String animation);

    void onAnimationResumed(String animation);

    void onAnimationStopped(String animation);

    void onAnimationFinished(String animation);
}
