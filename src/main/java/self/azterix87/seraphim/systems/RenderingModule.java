package self.azterix87.seraphim.systems;


import com.badlogic.ashley.core.Engine;
import self.azterix87.seraphim.RenderContext;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public interface RenderingModule {
    void addedToEngine(Engine entityEngine);

    void preRender(RenderContext renderContext, float dt);

    void render(int sortingIndex, RenderContext renderContext, float dt);

    void postRender(RenderContext renderContext, float dt);

    int getMinSortingIndex();

    int getMaxSortingIndex();
}
