package self.azterix87.seraphim.systems.collision;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;

public class CollisionContextBuilder {
    private Vector2 normal;
    private float depth;
    private boolean collisionWithStaticObject;
    private Entity currentEntity;
    private Entity anotherEntity;

    public CollisionContextBuilder setNormal(Vector2 normal) {
        this.normal = normal;
        return this;
    }

    public CollisionContextBuilder setDepth(float depth) {
        this.depth = depth;
        return this;
    }

    public CollisionContextBuilder setCurrentEntity(Entity currentEntity) {
        this.currentEntity = currentEntity;
        return this;
    }

    public CollisionContextBuilder setAnotherEntity(Entity anotherEntity) {
        this.anotherEntity = anotherEntity;
        return this;
    }

    public CollisionContextBuilder setCollisionWithStaticObject(boolean collisionWithStaticObject) {
        this.collisionWithStaticObject = collisionWithStaticObject;
        return this;
    }

    public CollisionContextBuilder invertNormal() {
        normal = new Vector2(normal).scl(-1);
        return this;
    }

    public CollisionContext createCollisionContext() {
        return new CollisionContext(normal, depth, currentEntity, anotherEntity, collisionWithStaticObject);
    }
}