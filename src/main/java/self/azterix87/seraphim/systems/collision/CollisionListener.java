package self.azterix87.seraphim.systems.collision;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public interface CollisionListener {
    void onCollision(CollisionContext collisionContext);
}
