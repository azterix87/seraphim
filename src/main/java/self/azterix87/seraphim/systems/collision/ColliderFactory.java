package self.azterix87.seraphim.systems.collision;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by azterix87 on 1/14/2017.
 * Project SeraphimEngine
 */
public class ColliderFactory {

    public static Collider getCircleCollider(float radius, float offsetX, float offsetY) {
        Circle circle = new Circle(offsetX, offsetY, radius);
        return new Collider(circle);
    }

    public static Collider getCircleCollider(float radius) {
        return getCircleCollider(radius, 0, 0);
    }

    public static Collider getBoxCollider(float width, float height, float offsetX, float offsetY) {
        Rectangle rectangle = new Rectangle(offsetX, offsetY, width, height);
        return new Collider(rectangle);
    }

    public static Collider getBoxCollider(float width, float height) {
        return getBoxCollider(width, height, 0, 0);
    }
}
