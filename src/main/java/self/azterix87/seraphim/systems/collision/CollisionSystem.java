package self.azterix87.seraphim.systems.collision;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.dynamics.Dynamics;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class CollisionSystem extends EntitySystem {
    private ImmutableArray<Entity> collidingEntities;

    public CollisionSystem(int priority) {
        super(priority);
    }

    private static CollisionContextBuilder circleXBox(Vector3 position1, Collider collider1, Vector3 position2, Collider collider2) {
        Circle mask1 = (Circle) collider1.getMask();
        Rectangle mask2 = (Rectangle) collider2.getMask();
        Circle realCircle = new Circle(position1.x + mask1.x, position1.y + mask1.y, mask1.radius);

        Vector2 maskPos1 = new Vector2(mask1.x, mask1.y).add(position1.x, position1.y);
        Vector2 maskPos2 = new Vector2(mask2.x, mask2.y).add(position2.x, position2.y);


        Vector2 collisionNormal = null;
        float collisionDepth = 0;


        if (maskPos1.x >= maskPos2.x && maskPos1.x <= maskPos2.x + mask2.width) {
            if (maskPos1.y <= maskPos2.y && maskPos1.y + mask1.radius >= maskPos2.y) {
                collisionNormal = new Vector2(0, -1);
                collisionDepth = maskPos1.y + mask1.radius - maskPos2.y;
            } else if (maskPos1.y >= maskPos2.y + mask2.height && maskPos1.y - mask1.radius <= maskPos2.y + mask2.height) {
                collisionNormal = new Vector2(0, 1);
                collisionDepth = maskPos2.y + mask2.height - (maskPos1.y - mask1.radius);
            }
        } else if (maskPos1.y >= maskPos2.y && maskPos1.y <= maskPos2.y + mask2.height) {
            if (maskPos1.x <= maskPos2.x && maskPos1.x + mask1.radius >= maskPos2.x) {
                collisionNormal = new Vector2(-1, 0);
                collisionDepth = maskPos1.x + mask1.radius - maskPos2.x;
            } else if (maskPos1.x >= maskPos2.x + mask2.width && maskPos1.x - mask1.radius <= maskPos2.x + mask2.width) {
                collisionNormal = new Vector2(1, 0);
                collisionDepth = maskPos2.x + mask2.width - maskPos1.x + mask1.radius;
            }
        } else {
            Vector2 BL = new Vector2(position2.x + mask2.x, position2.y + mask2.y);
            Vector2 TL = new Vector2(position2.x + mask2.x, position2.y + mask2.y + mask2.height);
            Vector2 BR = new Vector2(position2.x + mask2.x + mask2.width, position2.y + mask2.y);
            Vector2 TR = new Vector2(position2.x + mask2.x + mask2.width, position2.y + mask2.y + mask2.height);

            if (realCircle.contains(BL)) {
                collisionNormal = maskPos1.cpy().sub(BL).nor();
                collisionDepth = realCircle.radius - BL.dst(maskPos1);
            } else if (realCircle.contains(BR)) {
                collisionNormal = maskPos1.cpy().sub(BR).nor();
                collisionDepth = realCircle.radius - BR.dst(maskPos1);
            } else if (realCircle.contains(TL)) {
                collisionNormal = maskPos1.cpy().sub(TL).nor();
                collisionDepth = realCircle.radius - TL.dst(maskPos1);
            } else if (realCircle.contains(TR)) {
                collisionNormal = maskPos1.cpy().sub(TR).nor();
                collisionDepth = realCircle.radius - TR.dst(maskPos1);
            }
        }


        if (collisionNormal != null) {
            return new CollisionContextBuilder().setNormal(collisionNormal).setDepth(collisionDepth);
        }

        return null;
    }

    private static CollisionContextBuilder circleXCircle(Vector3 position1, Collider collider1, Vector3 position2, Collider collider2) {
        Circle mask1 = (Circle) collider1.getMask();
        Circle mask2 = (Circle) collider2.getMask();
        Vector2 displace = new Vector2(mask1.x + position1.x, mask1.y + position1.y).sub(mask2.x + position2.x, mask2.y + position2.y);
        float collisionDepth = mask1.radius + mask2.radius - displace.len();
        if (collisionDepth >= 0) {

            CollisionContextBuilder builder = new CollisionContextBuilder();

            builder.setDepth(collisionDepth);
            builder.setNormal(displace.nor());

            return builder;
        }

        return null;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        collidingEntities = engine.getEntitiesFor(Family.all(Transform.class, Collider.class).get());
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        for (int i = 0; i < collidingEntities.size(); i++) {
            for (int j = i + 1; j < collidingEntities.size(); j++) {
                checkAndHandleCollision(collidingEntities.get(i), collidingEntities.get(j));
            }
        }

    }

    public void checkAndHandleCollision(Entity e1, Entity e2) {
        Transform transform1 = Transform.mapper.get(e1);
        Collider collider1 = Collider.mapper.get(e1);
        Vector2 movement1 = Dynamics.mapper.has(e1) ? Dynamics.mapper.get(e1).movement : Vector2.Zero;

        Transform transform2 = Transform.mapper.get(e2);
        Collider collider2 = Collider.mapper.get(e2);
        Vector2 movement2 = Dynamics.mapper.has(e2) ? Dynamics.mapper.get(e2).movement : Vector2.Zero;


        Vector3 positionM1 = transform1.getGlobalPosition().cpy().add(movement1.x, movement1.y, 0);
        Vector3 positionM2 = transform2.getGlobalPosition().cpy().add(movement2.x, movement2.y, 0);

        CollisionContextBuilder contextBuilder = null;
        if (collider1.getMask() instanceof Circle) {
            if (collider2.getMask() instanceof Circle) {
                contextBuilder = circleXCircle(positionM1, collider1, positionM2, collider2);
            } else if (collider2.getMask() instanceof Rectangle) {
                contextBuilder = circleXBox(positionM1, collider1, positionM2, collider2);
            }
        } else if (collider1.getMask() instanceof Rectangle) {
            if (collider2.getMask() instanceof Circle) {
                contextBuilder = circleXBox(positionM2, collider2, positionM1, collider1);
                if (contextBuilder != null) contextBuilder.invertNormal(); //because objects are ordered incorrectly
            } else if (collider2.getMask() instanceof Rectangle) {
                //.System.out.println("other");
                //todo add rectangle x rextangle collision
            }
        }

        if (contextBuilder == null) {
            return; //no collision
        }


        CollisionContext context1 = contextBuilder.setCurrentEntity(e1).setAnotherEntity(e2)
                .setCollisionWithStaticObject(!Dynamics.mapper.has(e2)).createCollisionContext();

        CollisionContext context2 = contextBuilder.setCurrentEntity(e2).setAnotherEntity(e1).invertNormal()
                .setCollisionWithStaticObject(!Dynamics.mapper.has(e1)).createCollisionContext();

        collider1.onCollision(context1);
        collider2.onCollision(context2);
    }

    private boolean aabbCheck(Vector3 pos1, Rectangle m1, Vector3 pos2, Rectangle m2) {
        return pos1.x + m1.x < pos2.x + m2.x + m2.width
                && pos1.x + m1.x + m1.width > pos2.x + m2.x
                && pos1.y + m1.y < pos2.y + m2.y + m2.height
                && pos1.y + m1.y + m1.height > pos2.y + m2.y;
    }
}

