package self.azterix87.seraphim.systems.collision;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public class CollisionContext {
    private Vector2 normal;
    private float depth;
    private boolean collisionWithStaticObject;

    private Entity anotherEntity;
    private Entity currentEntity;

    protected CollisionContext(Vector2 normal, float depth, Entity currentEntity, Entity anotherEntity, boolean collisionWithStaticObject) {
        this.normal = normal;
        this.depth = depth;
        this.anotherEntity = anotherEntity;
        this.currentEntity = currentEntity;
        this.collisionWithStaticObject = collisionWithStaticObject;
    }

    public Vector2 getNormal() {
        return normal;
    }

    public void setNormal(Vector2 normal) {
        this.normal = normal;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    public boolean isCollisionWithStaticObject() {
        return collisionWithStaticObject;
    }

    public void setCollisionWithStaticObject(boolean collisionWithStaticObject) {
        this.collisionWithStaticObject = collisionWithStaticObject;
    }

    public Entity getAnotherEntity() {
        return anotherEntity;
    }

    public Entity getCurrentEntity() {
        return currentEntity;
    }
}
