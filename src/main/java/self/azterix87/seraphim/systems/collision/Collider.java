package self.azterix87.seraphim.systems.collision;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Shape2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public class Collider extends Component {
    public static final ComponentMapper<Collider> mapper = ComponentMapper.getFor(Collider.class);
    private Shape2D mask;
    private List<CollisionListener> listeners = new ArrayList<>();
    private boolean solid = true;

    protected Collider(Shape2D mask, boolean solid) {
        this(mask);
        this.solid = solid;
    }

    public Collider(Shape2D mask) {
        this.mask = mask;
    }

    public void onCollision(CollisionContext collisionContext) {
        for (CollisionListener listener : listeners) {
            listener.onCollision(collisionContext);
        }
    }

    public Shape2D getMask() {
        return mask;
    }

    public void addCollisionListener(CollisionListener listener) {
        listeners.add(listener);
    }

    public void removeCollisionListener(CollisionListener listener) {
        listeners.remove(listener);
    }

    public void clearCollisionListeners() {
        listeners.clear();
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }
}
