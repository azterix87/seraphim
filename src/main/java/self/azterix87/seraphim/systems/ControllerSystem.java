package self.azterix87.seraphim.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.ControllerHolder;

/**
 * Created by azterix87 on 7/3/2017.
 * Project WizardGame
 */
public class ControllerSystem extends IteratingSystem {
    private GameContext gameContext;

    public ControllerSystem(int priority, GameContext gameContext) {
        super(Family.all(ControllerHolder.class).get(), priority);
        this.gameContext = gameContext;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        engine.addEntityListener(new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                if (ControllerHolder.mapper.has(entity)) {
                    ControllerHolder.mapper.get(entity).onEntitySpawned(gameContext);
                }
            }

            @Override
            public void entityRemoved(Entity entity) {
                if (ControllerHolder.mapper.has(entity)) {
                    ControllerHolder.mapper.get(entity).onEntityRemoved(gameContext);
                }
            }
        });
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        ControllerHolder controller = ControllerHolder.mapper.get(entity);
        controller.update(deltaTime);
    }
}
