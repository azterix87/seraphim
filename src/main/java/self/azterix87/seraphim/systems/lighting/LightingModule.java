package self.azterix87.seraphim.systems.lighting;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimator;

/**
 * Created by azterix87 on 2/1/17.
 * Project SeraphimEngine
 */
public class LightingModule implements RenderingModule {
    private FrameBuffer lightFBO;
    private TextureRegion lightMap;
    private ImmutableArray<Entity> entities;
    private Vector3 ambientLight;

    public LightingModule(Vector3 ambientLight) {
        this.ambientLight = ambientLight;
        lightFBO = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        lightMap = new TextureRegion(lightFBO.getColorBufferTexture());
        lightMap.flip(false, true);
    }

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(Lightmap.class, Transform.class).get());
    }

    @Override
    public void preRender(RenderContext renderContext, float dt) {
        //Render lightmap
        lightFBO.begin();
        Gdx.gl.glClearColor(ambientLight.x, ambientLight.y, ambientLight.z, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        //renderContext.spriteBatch().setProjectionMatrix(renderContext.getMainCamera().combined);
        renderContext.spriteBatch().begin();
        renderContext.spriteBatch().enableBlending();
        renderContext.spriteBatch().setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_COLOR);

        for (Entity e : entities) {
            Lightmap lmap = Lightmap.mapper.get(e);
            Transform transform = Transform.mapper.get(e);

            renderContext.spriteBatch().setTransformMatrix(transform.getTransformMatrix());
            Texture texture = renderContext.assets().getTexture(lmap.getLightmap());

            float scale = lmap.getScale();

            float offX = lmap.getOffset().x - scale * texture.getWidth() / 2;
            float offY = lmap.getOffset().y - scale * texture.getHeight() / 2;


            if (MotionAnimator.mapper.has(e)) {
                Vector2 displace = MotionAnimator.mapper.get(e).currentDisplace;
                offX += displace.x;
                offY += displace.y;
            }


            renderContext.spriteBatch().draw(texture, offX, offY, texture.getWidth() * scale, texture.getHeight() * scale);
        }
        renderContext.spriteBatch().disableBlending();
        renderContext.spriteBatch().end();

        lightFBO.end();

    }

    @Override
    public void render(int sortingIndex, RenderContext renderContext, float dt) {

    }

    @Override
    public void postRender(RenderContext renderContext, float dt) {
        //apply lightmap
        renderContext.spriteBatch().begin();

        //We don't need mapper and view matrices for lightmap
        renderContext.spriteBatch().setTransformMatrix(new Matrix4());
        renderContext.spriteBatch().setProjectionMatrix(renderContext.getMainCamera().projection);

        renderContext.spriteBatch().setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        renderContext.spriteBatch().enableBlending();

        float scale = ((OrthographicCamera) renderContext.getMainCamera()).zoom;
        float width = ((OrthographicCamera) renderContext.getMainCamera()).viewportWidth;
        float height = ((OrthographicCamera) renderContext.getMainCamera()).viewportHeight;

        renderContext.spriteBatch().draw(lightMap, -width * scale / 2, -height * scale / 2,
                width * scale, height * scale);

        renderContext.spriteBatch().disableBlending();
        renderContext.spriteBatch().end();
    }

    @Override
    public int getMinSortingIndex() {
        return 0;
    }

    @Override
    public int getMaxSortingIndex() {
        return 0;
    }

}
