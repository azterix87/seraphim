package self.azterix87.seraphim.systems.lighting;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by azterix87 on 2/1/17.
 * Project SeraphimEngine
 */
public class Lightmap extends Component {
    public static final ComponentMapper<Lightmap> mapper = ComponentMapper.getFor(Lightmap.class);
    private String lightmap;
    private Vector2 offset;
    private float scale = 1;

    public Lightmap() {
        offset = new Vector2();
    }

    public Lightmap(String lightmap, Vector2 offset) {
        this.lightmap = lightmap;
        this.offset = offset;
    }

    public Lightmap(String lightmap, float offsetX, float offsetY) {
        this(lightmap, new Vector2(offsetX, offsetY));
    }

    public Lightmap(String lightmap) {
        this(lightmap, 0, 0);
    }

    public String getLightmap() {
        return lightmap;
    }

    public void setLightmap(String lightmap) {
        this.lightmap = lightmap;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public void setOffset(Vector2 offset) {
        this.offset = offset;
    }

    public void setOffset(float x, float y) {
        this.offset.set(x, y);
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
