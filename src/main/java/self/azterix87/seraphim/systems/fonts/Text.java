package self.azterix87.seraphim.systems.fonts;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by azterix87 on 2/5/17.
 * Project SeraphimEngine
 */
public class Text extends Component {
    public static final ComponentMapper<Text> mapper = ComponentMapper.getFor(Text.class);
    private String font;
    private String text;
    private Vector2 offset;

    public Text(String font) {
        this.font = font;
        this.offset = new Vector2();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public void setOffset(Vector2 offset) {
        this.offset = offset;
    }

}
