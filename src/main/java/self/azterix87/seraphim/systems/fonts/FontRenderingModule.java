package self.azterix87.seraphim.systems.fonts;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimator;

/**
 * Created by azterix87 on 2/5/17.
 * Project SeraphimEngine
 */
public class FontRenderingModule implements RenderingModule {
    private ImmutableArray<Entity> entities;

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(Transform.class, Text.class).get());
    }

    @Override
    public void preRender(RenderContext renderContext, float dt) {

    }

    @Override
    public void render(int sortingIndex, RenderContext renderContext, float dt) {

    }

    @Override
    public void postRender(RenderContext renderContext, float dt) {
        renderContext.spriteBatch().enableBlending();
        renderContext.spriteBatch().setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        renderContext.spriteBatch().setProjectionMatrix(renderContext.getMainCamera().combined);
        renderContext.spriteBatch().begin();

        for (Entity e : entities) {
            Transform transform = Transform.mapper.get(e);
            Text textC = Text.mapper.get(e);

            renderContext.spriteBatch().setTransformMatrix(transform.getTransformMatrix());

            Vector2 displace = textC.getOffset().cpy();

            if (MotionAnimator.mapper.has(e)) {
                displace.add(MotionAnimator.mapper.get(e).currentDisplace);
            }

            BitmapFont font = renderContext.assets().getFont(textC.getFont());

            font.draw(renderContext.spriteBatch(), textC.getText(), displace.x, displace.y);

        }

        renderContext.spriteBatch().end();
        renderContext.spriteBatch().disableBlending();
    }

    @Override
    public int getMinSortingIndex() {
        return 0;
    }

    @Override
    public int getMaxSortingIndex() {
        return 0;
    }

}
