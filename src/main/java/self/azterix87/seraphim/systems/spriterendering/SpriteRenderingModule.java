package self.azterix87.seraphim.systems.spriterendering;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.AnimationPlayer;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */

public class SpriteRenderingModule implements RenderingModule {
    private ImmutableArray<Entity> entities;
    private int minSortingIndex;
    private int maxSortingIndex;

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(SpriteRenderable.class, Transform.class).get());
    }

    @Override
    public void preRender(RenderContext renderContext, float dt) {
        //recalculate min and max sorting indexes
        for (Entity e : entities) {
            int sortingIndex = getSortingIndex(e);

            minSortingIndex = Math.min(minSortingIndex, sortingIndex);
            maxSortingIndex = Math.max(maxSortingIndex, sortingIndex);
        }
    }

    @Override
    public void render(int sortingIndex, RenderContext renderContext, float dt) {
        //find entities with current sorting index, and sort them by Y coordinate


        List<Entity> sortedEntities = new ArrayList<>();
        for (Entity e : entities) {
            if (getSortingIndex(e) == sortingIndex) {
                sortedEntities.add(e);
            }
        }
        sortedEntities.sort((o1, o2) -> (int) (Transform.mapper.get(o2).localPosition.y - Transform.mapper.get(o1).localPosition.y));

        //return if there are no entities with current sorting index
        if (sortedEntities.isEmpty()) {
            return;
        }


        //render current entities
        renderContext.spriteBatch().begin();
        renderContext.spriteBatch().setProjectionMatrix(renderContext.getMainCamera().combined);
        renderContext.spriteBatch().setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        renderContext.spriteBatch().enableBlending();

        for (Entity e : sortedEntities) {
            SpriteRenderable renderable = SpriteRenderable.mapper.get(e);

            updateSpriteAnimationPlayer(renderContext, renderable.getAnimationPlayer(), dt);


            if (!renderable.isVisible()) continue;

            Transform transform = Transform.mapper.get(e);
            renderContext.spriteBatch().setTransformMatrix(transform.getTransformMatrix());

            renderContext.spriteBatch().setColor(renderable.getColor());

            Vector2 displace = new Vector2(-renderable.getOriginX(), -renderable.getOriginY());

            if (MotionAnimator.mapper.has(e)) {
                displace.add(MotionAnimator.mapper.get(e).currentDisplace);
            }

            if (renderable.getAnimationPlayer().hasAnimation()) {
                Animation animation = renderContext.assets().getAnimation(renderable.getAnimationPlayer().getCurrentAnimation());
                TextureRegion frame = animation.getKeyFrame(renderable.getAnimationPlayer().getCurrentDuration());
                renderContext.spriteBatch().draw(frame, displace.x, displace.y);
            } else {
                renderContext.spriteBatch().draw(renderContext.assets().getTexture(renderable.getSprite()), displace.x, displace.y);
            }
            renderContext.spriteBatch().setColor(Color.WHITE);
        }

        renderContext.spriteBatch().disableBlending();
        renderContext.spriteBatch().end();
    }

    @Override
    public void postRender(RenderContext renderContext, float dt) {

    }

    @Override
    public int getMinSortingIndex() {
        return minSortingIndex;
    }

    @Override
    public int getMaxSortingIndex() {
        return maxSortingIndex;
    }

    private void updateSpriteAnimationPlayer(RenderContext renderContext, AnimationPlayer animationPlayer, float dt) {
        animationPlayer.update(dt);
        if (animationPlayer.hasAnimation()) {
            Animation animation = renderContext.assets().getAnimation(animationPlayer.getCurrentAnimation());
            if ((animation.getPlayMode() == Animation.PlayMode.NORMAL || animation.getPlayMode() == Animation.PlayMode.REVERSED)
                    && animation.isAnimationFinished(animationPlayer.getCurrentDuration())) {
                animationPlayer.finishAnimation();
            }
        }
    }

    private int getSortingIndex(Entity e) {
        Transform t = Transform.mapper.get(e);
        return (int) Math.floor(t.getGlobalPosition().z);
    }
}
