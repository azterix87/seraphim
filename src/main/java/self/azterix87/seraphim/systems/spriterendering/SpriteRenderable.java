package self.azterix87.seraphim.systems.spriterendering;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.Color;
import self.azterix87.seraphim.systems.AnimationPlayer;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class SpriteRenderable extends Component {
    public static final ComponentMapper<SpriteRenderable> mapper = ComponentMapper.getFor(SpriteRenderable.class);
    private String sprite;
    private AnimationPlayer spriteAnimationPlayer;
    private Color color = Color.WHITE;
    private float originX;
    private float originY;
    private boolean visible = true;
    //private boolean flipped;


    public SpriteRenderable() {
        spriteAnimationPlayer = new AnimationPlayer();
    }

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getOriginX() {
        return originX;
    }

    public void setOriginX(float originX) {
        this.originX = originX;
    }

    public float getOriginY() {
        return originY;
    }

    public void setOriginY(float originY) {
        this.originY = originY;
    }

    public AnimationPlayer getAnimationPlayer() {
        return spriteAnimationPlayer;
    }

    /*public boolean isFlipped() {
        return flipped;
    }

    public void setFlipped(boolean flipped) {
        this.flipped = flipped;
    }*/
}
