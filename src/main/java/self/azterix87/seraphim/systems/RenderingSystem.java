package self.azterix87.seraphim.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import self.azterix87.seraphim.RenderContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 4/30/17.
 * Project SeraphimEngine
 */
public class RenderingSystem extends EntitySystem {
    private RenderContext renderContext;
    private List<RenderingModule> modules = new ArrayList<>();

    public RenderingSystem(int priority, RenderContext renderContext) {
        super(priority);
        this.renderContext = renderContext;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        for (RenderingModule module : modules) {
            module.addedToEngine(engine);
        }
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        //Return if we have no rendering modules
        if (modules.isEmpty()) {
            return;
        }

        //PRE-RENDER
        for (RenderingModule module : modules) {
            module.preRender(renderContext, deltaTime);
        }


        //RENDER

        //Find out minimal and maximal sorting indexes in the scene
        int minSortingIndex = modules.get(0).getMinSortingIndex();
        int maxSortingIndex = modules.get(0).getMinSortingIndex();

        for (RenderingModule module : modules) {
            minSortingIndex = Math.min(minSortingIndex, module.getMinSortingIndex());
            maxSortingIndex = Math.max(maxSortingIndex, module.getMaxSortingIndex());
        }

        //Render modules with current Sorting index
        for (int i = minSortingIndex; i <= maxSortingIndex; i++) {
            for (RenderingModule module : modules) {
                if (i >= module.getMinSortingIndex() && i <= module.getMaxSortingIndex()) {
                    module.render(i, renderContext, deltaTime);
                }
            }
        }

        //POST-RENDER
        for (RenderingModule module : modules) {
            module.postRender(renderContext, deltaTime);
        }
    }

    public void addModule(RenderingModule renderingModule) {
        modules.add(renderingModule);
    }

    public void removeModule(RenderingModule renderingModule) {
        modules.remove(renderingModule);
    }
}
