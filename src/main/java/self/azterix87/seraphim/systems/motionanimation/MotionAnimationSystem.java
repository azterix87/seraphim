package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.systems.AnimationPlayer;

/**
 * Created by azterix87 on 2/7/17.
 * Project SeraphimEngine
 */
public class MotionAnimationSystem extends IteratingSystem {
    private RenderContext renderContext;

    public MotionAnimationSystem(int priority, RenderContext renderContext) {
        super(Family.all(MotionAnimator.class).get(), priority);
        this.renderContext = renderContext;
    }

    @Override
    protected void processEntity(Entity entity, float dt) {
        MotionAnimator animator = MotionAnimator.mapper.get(entity);
        updateMotionAnimationPlayer(renderContext, animator.getAnimationPlayer(), dt);

        AnimationPlayer player = animator.getAnimationPlayer();

        if (player.hasAnimation()) {
            MotionAnimation mainm = renderContext.assets().getMotionAnimation(player.getCurrentAnimation());
            animator.currentDisplace.set(mainm.getDisplace(player.getCurrentDuration()));
        } else {
            animator.currentDisplace.setZero();
        }
    }

    private void updateMotionAnimationPlayer(RenderContext renderContext, AnimationPlayer animationPlayer, float dt) {
        animationPlayer.update(dt);
        if (animationPlayer.hasAnimation()) {
            MotionAnimation animation = renderContext.assets().getMotionAnimation(animationPlayer.getCurrentAnimation());
            if (animation.isFinished(animationPlayer.getCurrentDuration())) {
                animationPlayer.finishAnimation();
            }
        }
    }

}
