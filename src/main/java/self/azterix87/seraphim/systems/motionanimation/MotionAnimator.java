package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.systems.AnimationListener;
import self.azterix87.seraphim.systems.AnimationPlayer;

/**
 * Created by azterix87 on 2/6/17.
 * Project SeraphimEngine
 */
public class MotionAnimator extends Component {
    public static final ComponentMapper<MotionAnimator> mapper = ComponentMapper.getFor(MotionAnimator.class);
    public final Vector2 currentDisplace;
    private AnimationPlayer animationPlayer;

    public MotionAnimator() {
        this.animationPlayer = new AnimationPlayer();
        this.currentDisplace = new Vector2();
    }

    public AnimationPlayer getAnimationPlayer() {
        return animationPlayer;
    }

    public void setAnimation(String animation) {
        animationPlayer.setAnimation(animation);
    }

    public void setAnimationAndPlay(String animation) {
        animationPlayer.setAnimationAndPlay(animation);
    }

    public void play() {
        animationPlayer.play();
    }

    public void pause() {
        animationPlayer.pause();
    }

    public void reset() {
        animationPlayer.reset();
    }

    public void stop() {
        animationPlayer.stop();
    }

    public boolean addListener(AnimationListener animationListener) {
        return animationPlayer.addListener(animationListener);
    }

    public boolean removeListener(AnimationListener animationListener) {
        return animationPlayer.removeListener(animationListener);
    }
}

