package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by azterix87 on 1/22/17.
 * Project SeraphimEngine
 */
public interface MotionAnimation {
    Vector2 getDisplace(float time);

    boolean isFinished(float time);
}
