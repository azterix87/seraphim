package self.azterix87.seraphim.systems.dynamics;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import self.azterix87.seraphim.components.Transform;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public class DynamicsSystem extends IteratingSystem {

    public DynamicsSystem(int priority) {
        super(Family.all(Dynamics.class, Transform.class).get(), priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = Transform.mapper.get(entity);
        Dynamics dynamics = Dynamics.mapper.get(entity);

        transform.localPosition.add(dynamics.movement.x, dynamics.movement.y, 0);
        dynamics.resetTickVariables();
    }
}
