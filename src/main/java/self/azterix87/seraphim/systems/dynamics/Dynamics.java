package self.azterix87.seraphim.systems.dynamics;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class Dynamics extends Component {
    public static final ComponentMapper<Dynamics> mapper = ComponentMapper.getFor(Dynamics.class);
    public final Vector2 movement = new Vector2();

    public void resetTickVariables() {
        movement.setZero();
    }
}
