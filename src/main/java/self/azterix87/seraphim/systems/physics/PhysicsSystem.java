package self.azterix87.seraphim.systems.physics;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.systems.dynamics.Dynamics;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class PhysicsSystem extends IteratingSystem {

    public PhysicsSystem(int priority) {
        super(Family.all(Dynamics.class, PhysicalBody.class).get(), priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PhysicalBody physicalBody = PhysicalBody.mapper.get(entity);
        Dynamics dynamics = Dynamics.mapper.get(entity);

        //apply drag
        Vector2 drag = physicalBody.velocity.cpy().nor().scl(-physicalBody.getDrag() * physicalBody.velocity.len() * deltaTime);

        //apply forces
        Vector2 force = physicalBody.getAppliedForce();
        physicalBody.applyImpulse(force.scl(deltaTime));

        //apply impulses
        Vector2 impulse = physicalBody.getAppliedImpulse();
        Vector2 lastVel = physicalBody.velocity.cpy();

        physicalBody.velocity.add(impulse.scl(1 / physicalBody.getMass()));
        if (physicalBody.velocity.len2() > drag.len2()) {
            physicalBody.velocity.add(drag);
        } else {
            physicalBody.velocity.setZero();
        }


        // dt * (v + v0) / 2
        Vector2 displace = (lastVel.cpy().add(physicalBody.velocity)).scl(deltaTime / 2);

        dynamics.movement.add(displace);

        physicalBody.resetTickVariables();
    }
}
