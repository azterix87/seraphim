package self.azterix87.seraphim.systems.physics;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class PhysicalBody extends Component {
    public static final ComponentMapper<PhysicalBody> mapper = ComponentMapper.getFor(PhysicalBody.class);
    public final Vector2 velocity = new Vector2();
    private final Vector2 appliedForce = new Vector2();
    private final Vector2 appliedImpulse = new Vector2();
    private float mass = 1;
    private float drag;

    public PhysicalBody() {

    }

    public void applyForce(Vector2 force) {
        appliedForce.add(force);
    }

    public void applyImpulse(Vector2 impulse) {
        appliedImpulse.add(impulse);
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public float getDrag() {
        return drag;
    }

    public void setDrag(float drag) {
        this.drag = drag;
    }

    public Vector2 getAppliedForce() {
        return appliedForce;
    }

    public Vector2 getAppliedImpulse() {
        return appliedImpulse;
    }

    public void resetTickVariables() {
        appliedForce.setZero();
        appliedImpulse.setZero();
    }
}
