package self.azterix87.seraphim.systems.physics;

import self.azterix87.seraphim.systems.collision.Collider;
import self.azterix87.seraphim.systems.collision.CollisionContext;
import self.azterix87.seraphim.systems.collision.CollisionListener;
import self.azterix87.seraphim.systems.dynamics.Dynamics;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public class PushOutCollisionHandler implements CollisionListener {

    @Override
    public void onCollision(CollisionContext collisionContext) {
        Collider collider1 = Collider.mapper.get(collisionContext.getCurrentEntity());
        Collider collider2 = Collider.mapper.get(collisionContext.getAnotherEntity());
        Dynamics dynamics = Dynamics.mapper.get(collisionContext.getCurrentEntity());


        if (dynamics != null && collider1.isSolid() && collider2.isSolid()) {
            if (collisionContext.isCollisionWithStaticObject()) {
                dynamics.movement.add(collisionContext.getNormal().cpy().scl(collisionContext.getDepth()));
            } else {
                dynamics.movement.add(collisionContext.getNormal().cpy().scl(collisionContext.getDepth() / 2));
            }
        }
    }
}
