package self.azterix87.seraphim;

/**
 * Created by azterix87 on 1/7/2017.
 * Project SeraphimEngine
 */
public interface Controller {
    void update(float delta);

    void onControllerAdded(GameContext gameContext);

    void onControllerRemoved(GameContext gameContext);
}
