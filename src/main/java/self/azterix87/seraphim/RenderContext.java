package self.azterix87.seraphim;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import self.azterix87.seraphim.assets.Assets;

/**
 * Created by azterix87 on 1/22/17.
 * Project SeraphimEngine
 */
public interface RenderContext {
    SpriteBatch spriteBatch();

    Camera getMainCamera();

    void setMainCamera(Camera camera);

    Assets assets();
}
