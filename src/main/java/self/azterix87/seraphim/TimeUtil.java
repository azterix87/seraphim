package self.azterix87.seraphim;

/**
 * Created by azterix87 on 5/30/2017.
 * Project Seraphim
 */
public class TimeUtil {
    public static float seconds() {
        return (float) (System.nanoTime() / 1_000_000_000D);
    }
}
