package self.azterix87.seraphim.components;


import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by azterix87 on 1/22/17.
 * Project SeraphimEngine
 */
public class Tags extends Component {
    public static final ComponentMapper<Tags> mapper = ComponentMapper.getFor(Tags.class);
    private Set<String> tags = new HashSet<>();

    public void addTag(String tag) {
        tags.add(tag);
    }

    public void removeTag(String tag) {
        tags.remove(tag);
    }

    public boolean hasTag(String tag) {
        return tags.contains(tag);
    }

}
