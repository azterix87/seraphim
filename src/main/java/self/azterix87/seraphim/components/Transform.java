package self.azterix87.seraphim.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public class Transform extends Component {
    public static final ComponentMapper<Transform> mapper = ComponentMapper.getFor(Transform.class);
    public final Vector3 localPosition = new Vector3();
    public final Vector2 localScale = new Vector2(1, 1);
    public float localRotation;
    private Transform parent;

    public Transform() {
    }

    public Transform getParent() {
        return parent;
    }

    public void setParent(Transform parent) {
        this.parent = parent;
    }

    public Matrix4 getTransformMatrix() {
        if (parent != null) {
            Matrix4 parentMatrix = parent.getTransformMatrix();
            parentMatrix.mul(getLocalTransformMatrix());
            return parentMatrix;
        } else {
            return getLocalTransformMatrix();
        }

    }

    public Matrix4 getLocalTransformMatrix() {
        Matrix4 matrix = new Matrix4();
        matrix.translate(localPosition);
        matrix.rotate(0, 0, 1, localRotation);
        matrix.scale(localScale.x, localScale.y, 1);
        return matrix;
    }

    public Vector3 getGlobalPosition() {
        if (parent == null) {
            return localPosition;
        } else {
            return localPosition.cpy().mul(parent.getTransformMatrix());
        }
    }
}
