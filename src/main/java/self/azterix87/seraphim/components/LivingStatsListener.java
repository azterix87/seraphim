package self.azterix87.seraphim.components;

/**
 * Created by azterix87 on 1/5/2017.
 * Project SeraphimEngine
 */
public interface LivingStatsListener {
    void onEntityDamaged(float amount, LivingStats livingStats);

    void onEntityHealed(float amount, LivingStats livingStats);

    void onEntityDied(LivingStats livingStats);
}
