package self.azterix87.seraphim.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;

/**
 * Created by azterix87 on 7/3/2017.
 * Project WizardGame
 */
public class ControllerHolder extends Component {
    public static ComponentMapper<ControllerHolder> mapper = ComponentMapper.getFor(ControllerHolder.class);

    private Controller controller;

    public ControllerHolder(Controller controller) {
        this.controller = controller;
    }

    public void update(float delta) {
        controller.update(delta);
    }

    public void onEntitySpawned(GameContext gameContext) {
        controller.onControllerAdded(gameContext);
    }

    public void onEntityRemoved(GameContext gameContext) {
        controller.onControllerRemoved(gameContext);
    }

    public Controller getController() {
        return controller;
    }
}
