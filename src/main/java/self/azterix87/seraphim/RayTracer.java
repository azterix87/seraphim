package self.azterix87.seraphim;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.collision.Collider;

/**
 * Created by azterix87 on 1/27/17.
 * Project SeraphimEngine
 */
public class RayTracer {
    public static float RAYTRACE_STEP = 0.1F;

    public static Entity raytrace(Vector2 start, Vector2 direction, float maxDistance, Entity[] entities) {
        Vector2 step = direction.cpy().scl(RAYTRACE_STEP);
        float stepsReal = maxDistance / step.len();
        int steps = (int) Math.ceil(stepsReal);
        //boolean hit = false;

        for (int i = 0; i < steps; i++) {
            float x = start.x + step.x * Math.min(i + 1, stepsReal);
            float y = start.y + step.y * Math.min(i + 1, stepsReal);

            for (Entity entity : entities) {

                if (Collider.mapper.has(entity)) {
                    Collider collider = Collider.mapper.get(entity);

                    Vector2 relativePos = new Vector2(x, y);
                    if (Transform.mapper.has(entity)) {
                        Vector3 entityPos = Transform.mapper.get(entity).localPosition;
                        relativePos.sub(entityPos.x, entityPos.y);
                    }

                    if (collider.getMask() instanceof Circle) {
                        if (((Circle) collider.getMask()).contains(relativePos)) {
                            return entity;
                        }
                    } else if (collider.getMask() instanceof Rectangle) {
                        if (((Rectangle) collider.getMask()).contains(relativePos)) {
                            return entity;
                        }
                    }
                }
            }
        }

        return null;
    }
}
